import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { setVisibilityFilter } from '~/actions'

class FilterForm extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
            subject: null,
            genre: null,
            grade: null,
            searchText: null
        }
    }
    
    /**
     * Handler for form submit event. Dispatches filter changes if needed.
     * @param   {object} e Event
     * @returns {boolean}
     */
    onFormSubmit(e) {
        const { dispatch, visibilityFilter } = this.props
        const state = this.state
        
        for (const key of Object.keys(state)) {
            const val = state[key];
            
            if (visibilityFilter[key] !== state[key]) {
                dispatch(setVisibilityFilter({
                    name: key,
                    value: val
                }))
            }            
        }
        
        e.preventDefault()
        return false
    }
    
    /**
     * Causes filterform to dispatch submit event.
     */
    submitForm() {
        this.refs.filterform.dispatchEvent(new Event('submit'))
    }
    
    /**
     * Changes state due to filter and submits form.
     * @param {object} e Event
     */
    onFilterSelected(e) {
        let filterName = e.target.getAttribute('name')
        
        this.setState({
            [filterName]: e.target.value
        }, () => {
            this.submitForm()
        });        
    }    
    
    /**
     * Changes state due to filter
     * @param {object} e Event
     */
    onSearchChange(e) {
        this.setState({
            searchText: e.target.value
        })
    }
    
    /**
     * React jsx nodes of subject select.
     * @returns {object}
     */
    subjectSelectNodes() {
        const { filterTypes } = this.props
        const { subjects } = filterTypes
        let options = []
        
        for (var k in subjects){
            if (typeof subjects[k] !== 'function') {
                 options.push((
                    <option key={k}>{k}</option>
                 ))
            }
        }
        
        return (
            <select className="form-control" id="subject" name="subject"
                onChange={this.onFilterSelected.bind(this)} style={{
                    width: '100%'
                }}>
                <option value="">Все предметы</option>
                {options}
            </select>
        )
    }
    
    /**
     * React jsx nodes of genre select.
     * @returns {object}
     */
    genreSelectNodes() {
        const { filterTypes } = this.props
        const { genres } = filterTypes
        let options = []
        
        for (var k in genres){
            if (typeof genres[k] !== 'function') {
                 options.push((
                    <option key={k}>{k}</option>
                 ))
            }
        }
        
        return (
            <select className="form-control" id="genre" name="genre" 
                onChange={this.onFilterSelected.bind(this)} style={{
                    width: '100%'
                }}>
                <option value="">Все жанры</option>
                {options}
            </select>
        )
    }
    
    /**
     * React jsx nodes of grade select.
     * @returns {object}
     */
    gradeSelectNodes() {
        const { filterTypes } = this.props
        const { grades } = filterTypes
        let options = []
        
        for (var k in grades){
            if (typeof grades[k] !== 'function') {
                 options.push((
                    <option key={k}>{k}</option>
                 ))
            }
        }
        
        return (
            <select className="form-control " id="grade" name="grade" 
                onChange={this.onFilterSelected.bind(this)} style={{
                    width: '100%'
                }}>
                <option value="">Все классы</option>
                {options}
            </select>
        )
    }

    render() {
        return (
<div className="container">        
    <form className="form-inline" ref="filterform" role="form" id="filterform" 
            style={{ maxWidth: '1120px' }} noValidate="novalidate"
            onSubmit={this.onFormSubmit.bind(this)}>
        <div className="form-group col-md-3 col-sm-4">        
            {this.subjectSelectNodes()}
        </div>
        <div className="form-group  col-md-3 col-sm-4">
            {this.genreSelectNodes()}
        </div>
        <div className="form-group  col-md-3 col-sm-4">
            {this.gradeSelectNodes()}
        </div>
        <div className="form-group col-md-3 col-sm-12" style={{
                whiteSpace: 'nowrap'                               
            }}>                    
            <input type="text" className="form-control" placeholder="Поиск" 
                id="search" name="search" 
                onChange={this.onSearchChange.bind(this)}
                style={{
                    width: '80%',
                    display: 'inline-block'                      
                }} />
            <button type="submit" className="btn btn-default " 
                style={{
                    width: '20%',
                    display: 'inline-block'   
                }}>
                <i className="fa fa-search"></i>
            </button>
        </div>
    </form>
</div>
        )
    }
}

FilterForm.propTypes = {
    filterTypes: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    const { filterTypes, visibilityFilter } = state
    
    return {
        filterTypes,
        visibilityFilter
    }
}

export default connect(mapStateToProps)(FilterForm)