import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchCoursesIfNeeded } from '~/actions'
import VisibleCourses from '~/components/VisibleCourses'
import FilterForm from '~/containers/FilterForm'

class ShowcaseApp extends Component {
    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchCoursesIfNeeded())
    }  

    render() {
        const { courses, isFetching, lastUpdated, visibilityFilter } = this.props
        return (
        <div className="row showcase-container">        
            {isFetching && courses.length === 0 &&
                <h2 className="text-center">Loading...</h2>
            }
            {!isFetching && courses.length === 0 &&
                <h2 className="text-center">Empty.</h2>
            }
            {courses.length > 0 &&
                <div style={{ opacity: isFetching ? 0.5 : 1 }}>
                    <FilterForm />
                    <VisibleCourses courses={courses} 
                        visibilityFilter={visibilityFilter} />
                </div>
            }
        </div>
        )
    }
}

ShowcaseApp.propTypes = {
    courses: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    lastUpdated: PropTypes.number,
    visibilityFilter: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    const { courses, visibilityFilter } = state
    const {
        isFetching,
        lastUpdated,
        items
    } = courses || {
        isFetching: true,
        items: []
    }

    return {
        courses: items,
        isFetching,
        lastUpdated,
        visibilityFilter
    }
}

export default connect(mapStateToProps)(ShowcaseApp)