import axios from 'axios'

export const REQUEST_COURSES = 'REQUEST_COURSES'
export const RECEIVE_COURSES = 'RECEIVE_COURSES'
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER'

function requestCourses() {
    return {
        type: REQUEST_COURSES
    }
}

function receiveCourses(data) {
    return {
        type: RECEIVE_COURSES,
        courses: data.items,
        receivedAt: Date.now()
    }
}

function fetchCourses() {
    return dispatch => {
        dispatch(requestCourses())
        return axios.post(`http://api.qa.imumk.ru/api/mobilev1/update`, {
            data: ''
        })        
        .then(response => response.data)
        .then(data => dispatch(receiveCourses(data)))
    }
}

function shouldFetchCourses(state) {
    const courses = state.items
    
    if (!courses) {
        return true
    } else if (courses.isFetching) {
        return false
    }
    
    return true
}

/**
 * Middleware function. Fetches courses from server if there is no cached data.
 * @returns {function} action creator
 */
export function fetchCoursesIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchCourses(getState())) {
            return dispatch(fetchCourses())
        }
    }
}

export const setVisibilityFilter = (filter) => {
    return {
        type: SET_VISIBILITY_FILTER,
        filter
    }
}