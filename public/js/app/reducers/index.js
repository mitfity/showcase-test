import { combineReducers } from 'redux'
import {
    REQUEST_COURSES, RECEIVE_COURSES,
    SET_VISIBILITY_FILTER
} from '~/actions'

const courses = (state = {
    isFetching: false,
    items: []
}, action) => {
    switch (action.type) {
        case REQUEST_COURSES:
            return Object.assign({}, state, {
                isFetching: true
        })
        case RECEIVE_COURSES:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.courses,
                lastUpdated: action.receivedAt
        })
        default:
            return state
    }
}

const collectFilterTypes = (courses) => {
    let types = {
        subjects: [],
        genres: [],
        grades: []
    }
    
    courses.forEach((el, index, coll) => {
        types.subjects[el.subject] = true
        types.genres[el.genre] = true
        
        let grades = el.grade ? el.grade.split(';') : ''
        for (let i = 0; i < grades.length; i++) {
            types.grades[grades[i]] = true
        }        
    })
    
    return types
}

const filterTypes = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_COURSES:
            return Object.assign({}, state, 
                                 collectFilterTypes(action.courses))
        default:
            return state
    }
}

const visibilityFilter = (state = {}, action) => {
    switch (action.type) {
        case SET_VISIBILITY_FILTER:
            return Object.assign({}, state, {
                [action.filter.name]: action.filter.value
        })
        default:
            return state
    }
}

const rootReducer = combineReducers({
    courses,
    visibilityFilter,
    filterTypes
})

export default rootReducer