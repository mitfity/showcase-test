import React, { PropTypes, Component } from 'react'
import Course from './Course'

export default class Courses extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
            priceType: 'rub'
        }
    }
    
    /**
     * Handles price type toggle change.
     * @param {object} e Event
     */
    onPriceChange(e) {
        let input = e.target.getElementsByTagName("input")[0];
        
        this.setState({
            priceType: input.value
        })
    }
    
    render() {
        return (
            <div className="container">
                <div className="price-toggle">
                    <div className="btn-group" data-toggle="buttons"
                        onClick={this.onPriceChange.bind(this)}>
                        <label className="btn btn-primary active">
                            <input type="radio" name="options"
                                autoComplete="off" value="rub" />
                            Рубли
                        </label>
                        <label className="btn btn-primary">
                            <input type="radio" name="options"
                                autoComplete="off" value="bonuses" />
                            Бонусы
                        </label>
                    </div>
                </div>
                
                {this.props.courses.map((course, i) =>
                    <Course course={course} key={i} 
                        priceType={this.state.priceType} />
                )}
            </div>
        )
    }
}

Courses.propTypes = {
    courses: PropTypes.array.isRequired
}