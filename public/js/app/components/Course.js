import React, { PropTypes, Component } from 'react'

export default class Course extends Component {
    
    constructor(props) {
        super(props)
        this.remoteUrl = 'http://xn----7sbbb6ahhdhybde.xn--p1ai/'
    }
    
    /**
     * React jsx course title node.
     * @returns {object}
     */
    titleNode() {
        const { course } = this.props
        let { grade } = course
        
        grade = grade ? grade.split(';') : ''
        let grades = grade ? 
            (grade.length ? (
                grade.length > 1 ? 
                    grade[0] 
                    + '-'
                    + grade[grade.length - 1]
                :
                    grade[0]
            ) : '') : ''
        
        return (
            <h3 className="sci-title">
                {course.subject},
                <span style={{ whiteSpace: 'nowrap' }}> {grades}</span>
            </h3>            
        )
    }
    
    /**
     * React jsx course image cover node.
     * @returns {object}
     */
    coverNode() {
        const { course } = this.props
        const { remoteUrl } = this
        
        let coverUrl = remoteUrl + 'covers/' + course.courseId + '.png'
        
        return (
            <div className="sci-cover">
                <img src={coverUrl} />
            </div>           
        )
    }
    
    /**
     * React jsx course content node.
     * @returns {object}
     */
    contentNode() {
        const { course } = this.props
        const { remoteUrl } = this
        
        let courseUrl = remoteUrl + 'course/' + course.courseId
        
        return (
            <a href ={courseUrl}>
                <div className="sci-cont">
                    {this.coverNode()}
                    {this.titleNode()}
                    <h4 className="sci-subtitle">{course.genre}</h4>
                    <p className="sci-desc">
                        {course.description || course.title}
                    </p>
                </div>
            </a>
        )
    }

    /**
     * React jsx course price text node.
     * @returns {object}
     */
    priceTextNode() {
        const { course, priceType } = this.props
        
        let price = course.price,
            priceTypeText = ' рублей'
        
        if (priceType === 'bonuses') {
            price = course.priceBonus
            priceTypeText = ' бонусов'
        }
        
        if (course.status === 'demo') {
            return null
        } else {
            return (
                <span> 
                    <b>{price}</b>
                    {priceTypeText}
                </span>                
            )
        }
    }

    /**
     * React jsx course buy/demo button node.
     * @returns {object}
     */
    buttonNode() {
        const { course } = this.props        
        const { remoteUrl } = this
        
        let buttonUrl = '#',
            buttonClass = 'sci-btn',
            buttonText = ''
            
        
        if (course.status === 'demo') {
            buttonUrl = remoteUrl + 'player/' + course.courseId
            buttonText = 'Перейти к обучению'
        } else {
            buttonClass += ' purchasebtn'
            buttonText = 'Приобрести за '
        }
        
        return (
            <a href={buttonUrl} target="_blank" className={buttonClass}>
                {buttonText}
                {this.priceTextNode()}
            </a>
        )
    }
    
    render() {
        const { course } = this.props
        
        return (
            <div className="sci">
                {this.contentNode()}
                {this.buttonNode()}
            </div>
        )
    }
}

Course.propTypes = {
    course: PropTypes.object.isRequired,
    priceType: PropTypes.string.isRequired
}