import React, { Component, PropTypes } from 'react'
import Courses from '~/components/Courses'

export default class VisibleCourses extends Component {
    
    /**
     * Checks if course matches search text.
     * @param   {object}   course
     * @param   {string}   searchText
     * @returns {boolean}  true if matches, false if not
     */
    matchesSearch(course, searchText) {
        let text = '',
            grades = course.grade ? course.grade.split(';') : ''
        
        text += course.title + ' ' + course.subject + ' ' + 
            course.description + ' ' + course.status + ' '
        
        for (let i = 0; i < grades.length; i++) {
            text += grades[i] + ' '
        }
        
        text = text.trim().toLowerCase()
        searchText = searchText.trim().toLowerCase()
        
        return (text.indexOf(searchText) > -1)
    }
    
    /**
     * Filters courses to match filters.
     * @returns {Array} Filtered courses
     */
    filterCourses() {
        let { courses, visibilityFilter } = this.props        
        
        courses = courses.filter((course, index, arr) => {
            for (const key of Object.keys(visibilityFilter)) {
                const filter = visibilityFilter[key];

                if (filter) {
                    if (key === 'grade') {
                        let grades = course.grade ? course.grade.split(';') : ''
                        let hasGrade = false
                        
                        for (let i = 0; i < grades.length; i++) {
                            if (filter === grades[i]) {
                                hasGrade = true
                            }
                        }
                        
                        if (!hasGrade) {
                            return false
                        }
                    } else if (key === 'searchText') {
                        if (!this.matchesSearch(course, filter)) {
                            return false
                        }
                    }  else if (course[key] !== filter) {
                        return false
                    }                                     
                }         
            }
            
            return true
        })
        
        return courses
    }

    render() {
        let courses = this.filterCourses()
        
        return (
            <Courses courses={courses} />
        )
    }
}

VisibleCourses.propTypes = {
    courses: PropTypes.array.isRequired
}